import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'layout',
    component: () => import('@/views/layout'),
    children: [
      {
        path: '',
        name: 'shouye',
        component: () => import('@/views/shouye')
      },
      {
        path: '/diandan',
        name: 'diandan',
        component: () => import('@/views/diandan')
      },
      {
        path: '/shangcheng',
        name: 'shangcheng',
        component: () => import('@/views/shangcheng')
      },
      {
        path: '/dingdan',
        name: 'dingdan',
        component: () => import('@/views/dingdan')
      }, {
        path: '/my',
        name: 'my',
        component: () => import('@/views/my')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
