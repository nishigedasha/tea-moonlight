import axios from 'axios'
const request = axios.create({
  baseURL: 'http://192.168.14.141:3001'
})
export const getLunBoTu = () => {
  return request({
    method: 'GET',
    url: '/lunbotu'
  })
}
